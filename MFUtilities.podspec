#
# Be sure to run `pod lib lint MFUtilities.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MFUtilities'
  s.version          = '0.0.2'
  s.summary          = 'A project to share common utility features'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Common utilities can be used in Any project. Provides fancy alerts, progress-huds, encryption, toast, app-language and others
                       DESC

  s.homepage         = 'https://bitbucket.org/ajkerfaisal/mfutilities'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mohammad Faisal' => 'ajkerfaisal@yahoo.com' }
  s.source           = { :git => 'https://ajkerfaisal@bitbucket.org/ajkerfaisal/mfutilities.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/ajkerfaisal'

  s.ios.deployment_target = '8.0'

  s.source_files = 'MFUtilities/Classes/**/*'
  
  # s.resource_bundles = {
  #   'MFUtilities' => ['MFUtilities/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  #s.frameworks = 'UIKit'
  #s.dependency 'SVProgressHUD'
  #s.dependency 'SIAlertView'
  #s.dependency 'CRToast', '~> 0.0.7'
end
