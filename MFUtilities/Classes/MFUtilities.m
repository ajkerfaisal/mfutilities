//
//  MFUtilities.m
//  Pods
//
//  Created by Mohammad.Faisal on 6/29/17.
//
//

#import "MFUtilities.h"

@implementation MFUtilities

    + (id)sharedInstance {
        static MFUtilities *sharedInstance = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance = [[self alloc] init];
        });
        return sharedInstance;
    }
    
    -(void) showAlertViewForTitle:(NSString *)title withMessage:(NSString*)message withCancel:(NSString*)cancel{
        [[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancel otherButtonTitles:nil, nil] show];
    }
    
//    - (NSString *) getMD5String: (NSString *) plainText{
//        const char *cStr = [plainText UTF8String];
//        unsigned char digest[16];
//        CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
//        NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
//        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
//        [output appendFormat:@"%02x", digest[i]];
//        return output;
//    }
    
    - (NSString *) getAppLanguage
    {
        NSString *language = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
        if([language isEqualToString:@"ar"])
        return language;
        else
        return @"en";
    }
    
    - (BOOL) isArabic
    {
        NSString *language = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
        if([language isEqualToString:@"ar"])
        return true;
        else
        return false;
    }
    
    - (float) getiOSVersion
    {
        float systemVersion = [[UIDevice currentDevice].systemVersion floatValue];
        if(systemVersion < 10)
        return true;
        else
        return false;
    }
    
    
@end
