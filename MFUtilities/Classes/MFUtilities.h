//
//  MFUtilities.h
//  Pods
//
//  Created by Mohammad.Faisal on 6/29/17.
//
//

#import <Foundation/Foundation.h>

@interface MFUtilities : NSObject

    + (id) sharedInstance;
    - (BOOL) isArabic;
    - (float) getiOSVersion;
    - (NSString *) getAppLanguage;
//    - (NSString *) getMD5String: (NSString *) plainText;
    - (void) showAlertViewForTitle:(NSString *)title withMessage:(NSString*)message withCancel:(NSString*)cancel;
@end
