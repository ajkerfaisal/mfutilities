//
//  MFAppDelegate.h
//  MFUtilities
//
//  Created by Mohammad.Faisal on 06/28/2017.
//  Copyright (c) 2017 Mohammad.Faisal. All rights reserved.
//

@import UIKit;

@interface MFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
