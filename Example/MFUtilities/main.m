//
//  main.m
//  MFUtilities
//
//  Created by Mohammad.Faisal on 06/28/2017.
//  Copyright (c) 2017 Mohammad.Faisal. All rights reserved.
//

@import UIKit;
#import "MFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MFAppDelegate class]));
    }
}
