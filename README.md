# MFUtilities

[![CI Status](http://img.shields.io/travis/Mohammad.Faisal/MFUtilities.svg?style=flat)](https://travis-ci.org/Mohammad.Faisal/MFUtilities)
[![Version](https://img.shields.io/cocoapods/v/MFUtilities.svg?style=flat)](http://cocoapods.org/pods/MFUtilities)
[![License](https://img.shields.io/cocoapods/l/MFUtilities.svg?style=flat)](http://cocoapods.org/pods/MFUtilities)
[![Platform](https://img.shields.io/cocoapods/p/MFUtilities.svg?style=flat)](http://cocoapods.org/pods/MFUtilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MFUtilities is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MFUtilities"
```

## Author

Mohammad.Faisal, ajkerfaisal@yahoo.com

## License

MFUtilities is available under the MIT license. See the LICENSE file for more info.
